$(document).ready(function() {
    
    /////////////////////////////////////////////
    ///////// STICKY NAV FUNCTION 
    /* we had the js--section-features class to every <section> elements 
    waypoint is a jQuery function that allows to scroll to the targeted element*/
    $('.js--section-features').waypoint(function(direction) {
        if(direction == "down") {
            $('nav').addClass('sticky delay-05s');
    } else {
            $('nav').removeClass('sticky');
    }
                                    
    }, {
        //to be perfectly aligned on the the title of the section during the automatic click-down-scroll
        offset: '60px;'}
                                        
    );
   

    /////////////////////////////////////////////
    ///////// MOBILE NAV STYLING
    /* give the good icon to the right type of nav (mobile/desktop)*/
    $('.js--mobile-nav-burger').click(function() {
        var nav = $('.main-nav') ;
        var icon = $('.js--mobile-nav-burger i');

        nav.slideToggle(200);

        if(icon.hasClass('ion-android-menu')) {
            icon.addClass('ion-close-round');
            icon.removeClass('ion-android-menu');
        } else {
            icon.addClass('ion-android-menu');
            icon.removeClass('ion-close-round');
        }

    }) ;


    /////////////////////////////////////////////
    ///////// SCROLLING ANIMATION FASTER
    $('.js--scroll-to-sign-up-section').click(function() {
        $('html,body').animate({scrollTop:$('.js--sign-up-section').offset().top},1000);
    }); 
    
    $('.js--scroll-to-section-features').click(function() {
        $('html,body').animate({scrollTop:$('.js--section-features').offset().top},1000);
    });
    
    
    /////////////////////////////////////////////
    ///////// SCROLL NAV BAR
    $('a[href*="#"]')
   // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
    // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
              // Figure out element to scroll to
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              // Does a scroll target exist?
              if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                      scrollTop: target.offset().top
                }, 1000, function() {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                  } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again 
                  };
                });
              }
           }
      });
    
    /////////////////////////////////////////////
    ///////// FADE IN EFFECT USED IN FEATURES SECTION
    $('.js--wp-1').waypoint(function() {
        $('.js--wp-1').addClass('animated slideInUp');
    },{
        offset : '50%;' 
    }
                           );    
    
    $('.js--wp-2').waypoint(function() {
        $('.js--wp-2').addClass('animated rollIn');
    },{
        offset : '50%;' 
    }
                           );
    
    $('.js--wp-3').waypoint(function() {
        $('.js--wp-3').addClass('animated shake');
    },{
        offset : '50%;' 
    }
                           );
    
});

